from selenium.webdriver.common.by import By


class HomePageLocators():

        # Home Page
        DONATE_NOW = "//div[text()='donate now']"
        NEWSLETTER_POPUP_CLOSE = "//div[@class='close-newsletterSign hidden-xs']/img[@src='img/cross.png']"
        EMAIL = "//input[@name='email']"
        PASSWORD = "//input[@name='new_password']"
        SUBMIT = "//button[text()='submit']"
        USER_ICON = "//span[@class='fas fa-user']"
        SIGNIN_SIGNUP_ACTIVE_TAB = "//div[@class='cdly-sidebar-section-account-item active']"
        SIGN_IN_TAB = "//div[contains(@class,'cdly-sidebar-section-account-item') and @data-name='signin']"
        SIGN_UP_TAB = "//div[contains(@class,'cdly-sidebar-section-account-item') and @data-name='signup']"


class WishlistsPageLocators():
     
        # Wishlists Page :
        READ_MORE_LIST = "//div[contains(text(),'Read More')]"
        PETS_NAME_LIST = "//div[contains(text(),'Read More')]//parent::div/div[@class='cdly-campaign-pet-name']"
        ITEMS_NEEDED_VALUE = "// div[contains(text(), 'pet_name')] // parent::div[ @class ='cdly-campaign-pet-info'] / div / div / div / div[@ class ='cdly-campaign-pet-highlight-value'][1]"
        DONATIONS_NEEDED_VALUE = "//div[contains(text(),'pet_name')]//parent::div[@class='cdly-campaign-pet-info']/div/div/div/div[@class='cdly-campaign-pet-highlight-value'][2]"
        FEATURED_WISHLIST_TITLE = "// div[contains(text(), 'Read More') and position() = 1] // parent::div // parent::div // div[ @class ='cdly-featured-wish-title']"
        CONTENT_HEADING = "// div[ @class ='grant-a-pw-content-heading']"
        CONTENT_SUBHEADING = "//div[@class='grant-a-pw-content-subheading']"
        CONTENT_DIVIDER = "//div[@class='grant-a-pw-content-divider-block']"
        CONTENT_TEXT = "//div[@class='grant-a-pw-content-content']"
        SEARCH_TEXT = "//div[@class='filter-label']"
        SEARCH_INPUT = "// input[ @ id = 'searchInput']"
        SEARCH_SUBMIT = "//div[@class='filter-go bg-primary']"
        SEARCH_IMAGE = "//div[@class='filter-go bg-primary']/span[@class='fa fa-search']"
        FEATURED_WISH_HEADER = "//div[@class='cdly-featured-wish-cta']"
        FEATURED_WISH_TITLE_TEXT = "//div[@class='cdly-featured-wish-title']"
        FEATURED_WISH_DESC_TEXT = "//div[@class='cdly-featured-wish-description']/p"
        PET_NAME_W_TEXT = "//div[@class='cdly-campaign-pet-name']"                        # Follwing xpaths give List inputs
        CAMPAIGN_DESC_TEXT = "//div[@class='cdly-campaign-pet-desc']"                       #
        MAP_MARKER_ICON = "//span[@class='fas fa-map-marker-alt']"                          #
        PET_LOCATION_TEXT = "//div[@class='cdly-campaign-pet-location']"                    #
        SAR_USER_ICON = "//div[@class='cdly-campaign-pet-organization']/span[@class='fas fa-user']"   #
        SAR_NAME_TEXT = "//div[@class='cdly-campaign-pet-organization']"
        PAGE_NEXT = "//a[text()='next']"                        #  "//a[@rel='next']"
        PAGE_PREV = "//a[@rel='prev']"
        PAGE_FIRST = "//a[contains(text(),'first')]"
        PAGE_LAST = "//a[contains(text(),'last')]"
        CURRENT_PAGE = "//li[@class='active']/a"


class PetDetailsPageLocators():

        # Pet Details Page
        CAMPAIGN_TITLE = "//div[@class='campaign-description']//span[@class='ng-binding']"
        PET_NAME = "//div[@class='campaign-container-other-details--pets campaign-container--pet-description campaign-background-white']//div[@class='campaign-pet-details--name ng-binding']"
        DONATE_BUTTON = "//button[contains(text(),'DONATE')]"
        COMPLETE_DONATION = "//input[@value='COMPLETE DONATION']"    # old checkout page
        CAMPAIGN_DESCR = "//div[@class='campaign-description']/span"
        DONATE_NOW = "//button[@class='campaign-give-btn hidden-sm hidden-xs']/span[text()='DONATE NOW']"
        AMT_DONATE =  "//button[contains(text(),'DONATE')]/parent::a"
        DONATE = "(//button[@ng-if=\"donationType == 'pet_donation'\"])[2]"                              #"(// button[contains(text(), 'DONATE')])[2]"
        DONATE_BUTTON_LINK = "//button[contains(text(),'DONATE')]/parent::a"
        PRODUCT_NAME_LIST ="// div[ @class ='shopify-product-card__content cuddly-list-shopping hidden-xs'] / div / div / div[@ class ='shopify-product-card__title wishlist-shopping--product cuddly-list-shopping ng-binding']"
        PRODUCT_DONATE_BUTTON = "//div[@class='shopify-product-card__qty-update']//button[contains(@ng-click,'cart')]"
        RESCUE_NAME = "(//div[@class ='campaign-container-other-details--rescue-name ng-binding ng-scope'])[2]"
        RESCUE_ADD = "//div[@class='campaign-container-other-details--rescue-location']/span"
        PAY_15 = "//button[contains(text(),15)]"   #"//a[@ng-repeat='paymentAmountOption in paymentAmountOptions']/button[contains(text(),15)]"
        PAY_35 = "//a[@ng-repeat='paymentAmountOption in paymentAmountOptions']/button[contains(text(),35)]"
        PAY_55 = "//a[@ng-repeat='paymentAmountOption in paymentAmountOptions']/button[contains(text(),55)]"
        PAY_105 = "//a[@ng-repeat='paymentAmountOption in paymentAmountOptions']/button[contains(text(),105)]"
        PAY_OTHER_BUTTON = "//button[contains(text(),'Other')]"
        PAY_OTHER_INPUT = "//input[@id ='donation-other-input--pet_donation']"
        CHECKOUT = "(//button[@ng-click='goToPayment()'])[1]"
        CART_ICON = "//span[@class='fas fa-shopping-cart']"
        CART_CLOSE = "(//div[@class='cdly-sidebar-menu-close wishlist-shopping-cart-sidebar-close ng-scope'])[1]"
        ALL_CART_ITEMS = "//div[@class='wishlist-shopping--cart-item']"  # divide by 2
        AMOUNT_UPDATE_DD = "(//select[@ng-change='changeDonationAmount(item)'])[1]" # Divide by 2
        CART_ITEMS_PRICE_LIST = "(// div[@class ='wishlist-shopping--cart-item-price wishlist-shopping-slider--cart-item-price ng-binding'])"
        CART_ITEMS_NAME_LIST = "//div[@class='wishlist-shopping--cart-item-product-title ng-binding']"


class CheckoutPageLocators():
        #  New Checkout Page        
        CC_NUMBER = "//input[@name='credit_card']"
        MMYY = "//input[@name='month_year']"
        CVV = "//input[@name='security_code']"
        DONOR_NAME = "//input[@name='name'][@id='name']"
        DONOR_EMAIL = "//div[@class='form-input']//input[@name='email']"
        DONOR_CONFIRM_EMAIL = "//input[@name='confirm_email']"
        STATE_DD = "//select[@name='state']"
        STRIPE_CC_RADIO = "//input[@value='stripe'][position()=1]"
        PAYPAL_RADIO = "//input[@type='radio' and @value='paypal']/parent::label/span" # "//input[@type='radio' and @value='paypal']"
        PAY_NOW = "(//button[@class='pay-now-button stripe-pay-now cartPay'])[2]"
        PAY_WITH_PAYPAL = "(//button[@class='pay-now-button cartPay'])[2]"
        TIP_SELECT = "//select[@name ='tipSelect']"


class PaypalLoginPageLocators():
        # Paypal Login Page
        PAYPAL_EMAIL = "//input[@id='email']"
        PAYPAL_PASS = "// input[ @ id = 'password']"
        PAYPAL_LOGIN = "//button[@id='btnLogin']"
        PAYPAL_NEXT = "//button[@name='btnNext']"
        PAYPAL_CONTINUE = "// input[ @ value = 'Continue']"



class ConfirmationPageLocators():
        # Confirmation Page :
        CONFIRM_MESSAGE = "//div[@class='donation-confirmation-modal-title']"
        CONFIRM_CLOSE = "//button[@aria-label='Close']"

        """" # DONATE_NOW = (By.XPATH, "//div[text()='donate now']")
        # DONATE_NOW = driver.find_element_by_xpath("//div[text()='donate now']")
        confirmationMessage = (By.XPATH, '//div[@class="donation-confirmation-modal-title"]')
        confirm_close = (By.XPATH, '//button[@aria-label="Close"]')
        campaignTitle = (By.XPATH, '//div[@class="campaign-description"]//span[@class="ng-binding"]')
        petName = (By.XPATH, '//div[@class="campaign-container-other-details--pets campaign-container--pet-description campaign-background-white"]//div[@class="campaign-pet-details--name ng-binding"]')
        donate = (By.XPATH, '//button[contains(text(),"DONATE")]')
        cc_number = (By.XPATH, '//input[@name="credit_card"]')
        mmyy = (By.XPATH, '//input[@name="month_year"]')
        cvv = (By.XPATH, '//input[@name="security_code"]')
        donorName = (By.XPATH, '//input[@name="name"][@id="name"]')
        donorEmail = (By.XPATH, '//div[@class="input custom-input email required"]//input[@name="email"]')
        donorEmailConfirm = (By.XPATH, '//input[@name="confirm_email"]')
        selState = (By.XPATH, '//select[@name="state"]')
        completDonation = (By.XPATH, '//input[@value="COMPLETE DONATION"]')
        ele_list = (By.XPATH, '//div[@class="cdly-xs-100 cdly-sm-50 cdly-md-33"]')
        """


class SARRegistrationPage():
        SAR_EMAIL = "(//input[@name='email' and @placeholder='Email Address'])[1]"
        SAR_PASS = "(//div[@class='input custom-input password required']/input[@name='new_password'])[1]"
        SAR_TYPE_SELECT = "(//select[@name='role'])[1]"
        LOGIN_LINK = "(//span[@class='sar-reg-card__minor-info']/a[@href='/login'])[1]"
        PROCEED_BUTTON = "(//b[contains(text(),'Proceed')]/parent::button)[1]"
        SAR_NAME = "//input[@placeholder='Shelter/Rescue Name']"
        EIN_NO = "//input[@name='ein_number']"
        COMPLETE_REGISTRATION = "//button[contains(text(),'Complete Registration ')]"
        BACK_BUTTON = "//button[contains(text(),' Back')]"


class UserRegistrationPage():
        EMAIL = "(//input[@placeholder='Email Address'])[1]"
        CONFIRM_EMAIL = "(//input[@name='email_confirm'])[1]"
        PASSWORD = "(//div[@class='input custom-input password required']/input[@name='new_password'])[1]"
        PROCEED_BUTTON = "(//button[contains(text(),'Proceed')])[1]"
        LOGIN_LINK = "(//span[@class='sar-reg-card__minor-info']/a[@href='/login'])[1]"


class LoginPage():
        EMAIL_ID = "//div[@class='input custom-input email required']/input[@name='email']"
        PASSWORD = "//div[@class='input custom-input password']/input[@name='new_password']"
        LOG_IN = "//button[contains(text(),'LOG IN')]"










