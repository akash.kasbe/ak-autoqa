import json

class DataReader:

    def read_data(self, filepath):
        with open(filepath,"r") as read_file:
            data = json.load(read_file)

        return data
