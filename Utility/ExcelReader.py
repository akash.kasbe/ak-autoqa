from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
import xlrd



class OrderedByName():
    """
    Privides a generator method, to iterate in Column Name ordered sequence
    Provides subscription, to get columns index by name. using class[name]
    """
    def __init__(self, sheet, fieldnames, row=0):
        """
        Create a OrderedDict {name:index} from 'fieldnames'
        :param sheet: The Worksheet to use
        :param fieldnames: Ordered List of Column Names
        :param row: Default Row Index for the Header Row
        """
        from collections import OrderedDict
        self.columns = OrderedDict().fromkeys(fieldnames, None)
        for n in range(sheet.ncols):
            self.columns[sheet.cell(row, n).value] = n

    @property
    def ncols(self):
        """
        Generator, equal usage as range(xlrd.ncols),
          to iterate columns in ordered sequence
        :return: yield Column index
        """
        for idx in self.columns.values():
            yield idx

    def __getitem__(self, item):
        """
        Make class object subscriptable
        :param item: Column Name
        :return: Columns index
        """
        return self.columns[item]

    def get_excel_data(self):
        file_location = r"/Users/akashkasbe/Documents/Akash/autoqa_ak/autoqa/autoqa/autoqa_func/Resources/elementsData.xlsx"
        workbook = xlrd.open_workbook(file_location)
        sheet = workbook.sheet_by_index(0)
        print(sheet.ncols, sheet.nrows, sheet.name, sheet.number)

        # Instantiate with Ordered List of Column Names
        # NOTE the different Order of Column Names
        by_name = OrderedByName(sheet, ['xpath', '//*[@id="uh-search-box"]', '//*[@id="uh-signin"]'])

        # Iterate all Rows and all Columns Ordered as instantiated
        for row in range(sheet.nrows):
            for col in by_name.ncols:
                value = sheet.cell(row, col).value

        pet_to_donate = sheet.cell(1, by_name['pet_to_donate']).value
        amount = sheet.cell(1, by_name['amount']).value
        donate_product = sheet.cell(1, by_name['donate_product']).value
        payment_option = sheet.cell(1, by_name['payment_option']).value
        cc_no = sheet.cell(1, by_name['cc_no']).value
        mm_yy = sheet.cell(1, by_name['mm_yy']).value
        cvv = sheet.cell(1, by_name['cvv']).value
        donor_email = sheet.cell(1, by_name['donor_email']).value
        state_code = sheet.cell(1, by_name['state_code']).value

        data = [pet_to_donate, amount, donate_product, payment_option, cc_no, mm_yy, cvv, donor_email, state_code]

        return  data




