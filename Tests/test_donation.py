from selenium import webdriver
import time
import json
import unittest

from Pages.DonationPages.WishlistsPage import WishlistsPage
from Pages.DonationPages.ConfirmationPage import ConfirmationPage
from Pages.DonationPages.CheckoutPage import CheckoutPage
from Pages.DonationPages.PetDetailsPage import PetDetailsPage
from Pages.DonationPages.HomePage import HomePage
import definitions
from Tests.TestBase.TestBase import TestBase


class DonationTest(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        TestBase.open_browser(cls, "chrome")


    def test_donation(self):

        with open(definitions.ROOT_DIR+"/Resources/TestData/data.json", "r") as read_file:
            get_data = json.load(read_file)

        driver = TestBase.get_driver(self)
        url = get_data['url']
        driver.get(url)

        home_page = HomePage(driver)
        home_page.click_donate_now()

        time.sleep(5)
        wishlists_page = WishlistsPage(driver)
        wishlists_page.pet_to_donate(get_data['pet_to_donate'])

        time.sleep(5)

        pet_details_page = PetDetailsPage(driver)

        pet_details_page.donate_amount(get_data['amount'])
        pet_details_page.donate_product(get_data['donate_product'])

        time.sleep(5)
        pet_details_page.open_cart()
        time.sleep(5)
        pet_details_page.click_checkout()

        time.sleep(5)

        checkout_page = CheckoutPage(driver)
        checkout_page.select_payment_option(get_data['payment_option'])
        checkout_page.enter_user_details(get_data)

        time.sleep(10)
        confirmation_page = ConfirmationPage(driver)
        #confirmation_page.close_confirm_popup()

    @classmethod
    def tearDownClass(cls):
        print("Script Executed !!")
        TestBase.close_browser(cls)












