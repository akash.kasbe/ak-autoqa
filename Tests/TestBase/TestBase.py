from selenium import webdriver
import os
import definitions


class TestBase:

    driver = None

    def get_driver(self):
        return self.driver

    def detect_os(self):
        OS = definitions.OS
        return OS

    def open_browser(self, browser):

        if browser.lower() == "firefox":
            print("Testing on Firefox browser...")

            self.driver = webdriver.Firefox(
                executable_path=definitions.ROOT_DIR +"/Resources/Drivers/"+TestBase.detect_os(self)+"/geckodriver")  # get filepath from config filew

        elif browser.lower() == "chrome":
            print("Testing on Chrome browser...")

            self.driver = webdriver.Chrome(
                executable_path=definitions.ROOT_DIR +"/Resources/Drivers/"+TestBase.detect_os(self)+"/chromedriver")  # get filepath from config file

        elif browser.lower() == "safari":
            print("Testing on Safari browser...")
        # safari driver filepath

        # Code for other browsers ...

        else:
            print("Testing on Chrome browser as a Default browser, since browser was not detected in Testcase")

            self.driver = webdriver.Chrome(
                executable_path=definitions.ROOT_DIR +"/Resources/Drivers/"+TestBase.detect_os(self)+"/chromedriver")

        TestBase.maximize_browser(self)
        TestBase.delete_cookies(self)

        return self.driver

    def maximize_browser(self):
        self.driver.maximize_window()

    def delete_cookies(self):
        self.driver.delete_all_cookies()

    def close_browser(self):
        self.driver.close()

