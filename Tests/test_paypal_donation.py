from selenium import webdriver
import time
import json
import unittest

from Pages.DonationPages.HomePage import HomePage
from Pages.DonationPages.WishlistsPage import WishlistsPage
from Pages.DonationPages.ConfirmationPage import ConfirmationPage
from Pages.DonationPages.CheckoutPage import CheckoutPage
from Pages.DonationPages.PetDetailsPage import PetDetailsPage
from Pages.DonationPages.PaypalLoginPage import PaypalLoginPage

from Tests.TestBase.TestBase import TestBase
import definitions


class PaypalDonation(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        TestBase.open_browser(cls, "chrome")

    def test_paypal_donation(self):
        with open(definitions.ROOT_DIR+"/Resources/TestData/data3.json", "r") as read_file:
            get_data = json.load(read_file)

        driver = self.driver
        url = get_data['url']
        self.driver.get(url)
        print("URL entered !!!")

        print("driver.title", self.driver.title)

        home_page = HomePage(driver)
        home_page.click_donate_now()

        time.sleep(5)
        wishlists_page = WishlistsPage(driver)
        wishlists_page.pet_to_donate(get_data['pet_to_donate'])

        time.sleep(5)

        pet_details_page = PetDetailsPage(driver)
        #pet_details_page.click_donate_now()
        pet_details_page.donate_amount(get_data['amount'])
        pet_details_page.donate_product(get_data['product'])

        '''
        donate = self.driver.find_element_by_xpath("//button[contains(text(),'DONATE')]")

        actions = Actions(self.driver)
        actions.move_to_element(donate)
        WebDriverWait(self.driver, 15).until(
            EC.element_to_be_clickable((By.XPATH, "//button[contains(text(),'DONATE')]/parent::a")))
        actions.move_to_element(donate)
        donate2 = self.driver.find_element_by_xpath("//button[contains(text(),'DONATE')]/parent::a")

        print(donate.location)
        print(donate2.location)
        driver.execute_script("arguments[0].click();", donate)
        #donate2.click()                         # send_keys(Keys.ENTER)  # (724, 966)
        '''
        time.sleep(5)

        pet_details_page.click_checkout()

        time.sleep(5)

        checkout_page = CheckoutPage(driver)
        checkout_page.select_tip(get_data['tip'])
        time.sleep(3)
        checkout_page.select_payment_option(get_data['select_payment_option'])
        time.sleep(3)
        checkout_page.donate_with_paypal()

        time.sleep(3)

        paypal_login = PaypalLoginPage(driver)
        paypal_login.login_to_paypal(get_data['paypal_username'], get_data['paypal_pass'])

        time.sleep(15)
        confirmation_page = ConfirmationPage(driver)
        #confirmation_page.close_confirm_popup()

    @classmethod
    def tearDownClass(cls):
        print("Script Executed !!")
        TestBase.close_browser(cls)

if __name__ == '__main__':
    unittest.main()




