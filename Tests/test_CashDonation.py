import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import  Keys
import time
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By

from Pages.DonationPages.HomePage import HomePage
from Pages.DonationPages.WishlistsPage import WishlistsPage
from Pages.DonationPages.ConfirmationPage import ConfirmationPage
from Pages.DonationPages.CheckoutPage import CheckoutPage
from Pages.DonationPages.PetDetailsPage import PetDetailsPage

from Tests.TestBase.TestBase import TestBase
import definitions
import json


class CashDonation(unittest.TestCase):

    @classmethod
    def setUpClass(cls):

        TestBase.open_browser(cls, "chrome")

    def test_CashDonation(self):

        with open(definitions.ROOT_DIR+"/Resources/TestData/data2.json", "r") as read_file:
            get_data = json.load(read_file)

        driver = self.driver

        url = get_data['url']
        driver.get(url)

        home_page = HomePage(driver)
        home_page.click_donate_now()

        time.sleep(5)
        wishlists_page = WishlistsPage(driver)
        assert(wishlists_page.get_title() == "Wishlists on CUDDLY")
        wishlists_page.pet_to_donate(get_data['pet_to_donate'])

        pet_details_page = PetDetailsPage(driver)
        time.sleep(20)
        
        assert(pet_details_page.get_title().lower() == pet_details_page.get_pet_name().lower()+" "+pet_details_page.get_campaign_name().lower())
        pet_details_page.donate_amount(get_data['amount'])
        pet_details_page.click_checkout()

        time.sleep(20)
        checkout_page = CheckoutPage(driver)
        checkout_page.enter_user_details(get_data)
        time.sleep(7)

        confirmation_page = ConfirmationPage(driver)
        #confirmation_page.get_confirmation_message()
        #confirmation_page.close_confirm_popup()

    @classmethod
    def tearDownClass(cls):
        print("Script Executed !!")
        TestBase.close_browser(cls)


