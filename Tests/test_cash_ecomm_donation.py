import time
import unittest

from Pages.DonationPages.HomePage import HomePage
from Pages.DonationPages.WishlistsPage import WishlistsPage
from Pages.DonationPages.ConfirmationPage import ConfirmationPage
from Pages.DonationPages.CheckoutPage import CheckoutPage
from Pages.DonationPages.PetDetailsPage import PetDetailsPage

from Tests.TestBase.TestBase import TestBase
from Utility.TestDataReader import DataReader
import definitions


class CashEcommDonation(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        TestBase.open_browser(cls, "chrome")

    def test_cash_ecomm_donation(self):
        filepath = definitions.ROOT_DIR+"/Resources/TestData/data2.json"

        get_data = DataReader.read_data(self, filepath)

        driver = TestBase.get_driver(self)
        url = get_data['url']
        driver.get(url)

        home_page = HomePage(driver)
        home_page.click_donate_now()

        time.sleep(5)
        wishlists_page = WishlistsPage(driver)
        wishlists_page.pet_to_donate(get_data['pet_to_donate'])

        time.sleep(5)

        pet_details_page = PetDetailsPage(driver)

        pet_details_page.donate_amount(get_data['amount'])
        pet_details_page.donate_product(get_data['donate_product'])

        time.sleep(5)

        pet_details_page.click_checkout()

        time.sleep(5)

        checkout_page = CheckoutPage(driver)
        checkout_page.select_payment_option(get_data['select_payment_option'])
        checkout_page.enter_user_details(get_data)

        time.sleep(15)
        confirmation_page = ConfirmationPage(driver)
        #confirmation_page.close_confirm_popup()

    @classmethod
    def tearDownClass(cls):
        print("Script Executed !!")
        TestBase.close_browser(cls)










