from selenium import webdriver
import time
import json
import unittest

from Pages.DonationPages.HomePage import HomePage
from Pages.DonationPages.WishlistsPage import WishlistsPage
from Pages.DonationPages.ConfirmationPage import ConfirmationPage
from Pages.DonationPages.CheckoutPage import CheckoutPage
from Pages.DonationPages.PetDetailsPage import PetDetailsPage

from Tests.TestBase.TestBase import TestBase
import definitions


class PaymentOptionDonation(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        TestBase.open_browser(cls, "chrome")

    def test_payment_option(self):
        with open(definitions.ROOT_DIR+"/Resources/TestData/data2.json", "r") as read_file:
            get_data = json.load(read_file)

        driver = self.driver

        url = get_data['url']
        driver.get(url)

        home_page = HomePage(driver)
        home_page.click_donate_now()

        wishlists_page = WishlistsPage(driver)
        wishlists_page.pet_to_donate(get_data['pet_to_donate'])

        pet_details_page = PetDetailsPage(driver)
        pet_details_page.donate_amount(get_data['amount'])
        pet_details_page.click_checkout()

        time.sleep(20)
        petdonation_page = CheckoutPage(driver)
        petdonation_page.select_payment_option(get_data['select_payment_option'])
        petdonation_page.enter_user_details(get_data)

        confirmation_page = ConfirmationPage(driver)
        #confirmation_page.get_confirmation_message()

    @classmethod
    def tearDownClass(cls):
        print("Script Executed !!")
        TestBase.close_browser(cls)


