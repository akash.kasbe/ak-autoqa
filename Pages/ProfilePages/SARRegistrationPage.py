from Pages.BasePage import Page
from Resources.Locators.locators import SARRegistrationPage as locator
from selenium.webdriver.support.select import Select
from selenium.webdriver.chrome.webdriver import WebDriver


class SARRegistrationPage(Page):

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def register_sar(self, data):
        SARRegistrationPage.enter_sar_creds(self, data.email, data.passwd)
        SARRegistrationPage.select_sar_type(data.sar_type)
        SARRegistrationPage.click_proceed()
        SARRegistrationPage.enter_sar_info(self, data.sar_name, data.emp_id)
        SARRegistrationPage.click_complete_registration()

    def enter_sar_creds(self, data):
        email_id = self.driver.find_element_by_xpath(locator.SAR_EMAIL)
        email_id.send_keys(data.email)

        password = self.driver.find_element_by_xpath(locator.SAR_PASS)
        password.send_keys(data.passwd)

    def select_sar_type(self, data):
        sar_type_dd = self.driver.find_element_by_xpath(locator.SAR_TYPE_SELECT)
        sar_type = Select(sar_type_dd)

        sar_type.select_by_visible_text(data.sar_type_value)

    def click_proceed(self):
        proceed = self.driver.find_element_by_xpath(locator.PROCEED_BUTTON)
        proceed.click()

    def enter_sar_info(self, data):
        sar_name = self.driver.find_element_by_xpath(locator.SAR_NAME)
        sar_name.send_keys(data.sar_name)

        emp_no = self.driver.find_element_by_xpath(locator.EIN_NO)
        emp_no.send_keys(data.emp_id)

    def click_complete_registration(self):
        complete_registration = self.driver.find_element_by_xpath(locator.COMPLETE_REGISTRATION)
        complete_registration.click()

    def click_back_button(self):
        back = self.driver.find_element_by_xpath(locator.BACK_BUTTON)
        back.click()

    def click_login_link(self):
        login = self.driver.find_element_by_xpath(locator.LOGIN_LINK)
        login.click()






