from Resources.Locators.locators import WishlistsPageLocators as locator
import time
from selenium.webdriver.chrome.webdriver import WebDriver
from Pages.BasePage.Page import Page


class WishlistsPage(Page):

    # def __init__(self, driver: WebDriver):
    #     self.driver = driver

    def pet_to_donate(self, pet_to_donate):
        time.sleep(10)

        read_more_list = self.driver.find_elements_by_xpath(locator.READ_MORE_LIST)

        print("No. of pets present on page no. "+WishlistsPage.get_current_page_no(self)+" :"+str(read_more_list.__len__()))

        pet_name_list = self.driver.find_elements_by_xpath(locator.PETS_NAME_LIST)
        count = 0

        for pet_name in pet_name_list:
            if pet_to_donate == pet_name.text and pet_name.text is not None:
                WishlistsPage.get_pet_info(self, pet_to_donate)
                print(pet_to_donate+" is found on Page #"+WishlistsPage.get_current_page_no(self))
                pet = self.driver.find_element_by_xpath("//div[ @class ='cdly-campaign-pet-name' and contains(text(), '"+pet_to_donate+"')]")
                pet.click()
                break

            else:
                count += 1

                if count == len(pet_name_list):
                    WishlistsPage.go_to_next_page(self)
                    WishlistsPage.pet_to_donate(self, pet_to_donate)

        self.driver.implicitly_wait(10)

    def get_featured_wish_title(self):
        featured_wish_title = self.driver.find_element_by_xpath(locator.FEATURED_WISHLIST_TITLE)
        print("Featured_wish_title : " + featured_wish_title.text)

    def get_pet_info(self, pet_name):  # Need to modify method
        items_xpath = locator.ITEMS_NEEDED_VALUE
        new_xpath = items_xpath.replace("pet_name", pet_name)
        items = self.driver.find_elements_by_xpath(new_xpath)
        print("Items needed for ", pet_name + " : ", items[0].text)
        print("Donations needed for ", pet_name + " : ", items[1].text)

    def donate_to_featured_wish_pet(self):
        featured_wish = self.driver.find_element_by_xpath(locator.FEATURED_WISH_HEADER)
        featured_wish.click()

    def go_to_page(self, page_no):
        self.driver.get("https://qa.cuddly.com/wishlists?page="+str(page_no))

    def go_to_next_page(self):
        next_page = self.driver.find_element_by_xpath(locator.PAGE_NEXT)

        if next_page.is_enabled():
            next_page.click()
            time.sleep(10)

        else:
            print("Searched all the Pages! The Pet is not found.")

    def get_current_page_no(self):
        current_page = self.driver.find_element_by_xpath(locator.CURRENT_PAGE)
        return current_page.text


