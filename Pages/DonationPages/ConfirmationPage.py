from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
import time
from Resources.Locators.locators import ConfirmationPageLocators as locator

from Pages.BasePage.Page import Page


class ConfirmationPage(Page):

    # def __init__(self, driver):
    #     self.driver = driver

    def close_confirm_popup(self):
        time.sleep(3)
        confirm_close = self.driver.find_element_by_xpath(locator.CONFIRM_CLOSE)
        confirm_close.click()

    def get_confirmation_message(self):
        WebDriverWait(self.driver, 20).until(
            expected_conditions.presence_of_element_located((By.XPATH, locator.CONFIRM_MESSAGE)))

        confirmation_message = self.driver.find_element_by_xpath(locator.CONFIRM_MESSAGE)
        print(confirmation_message.text)
