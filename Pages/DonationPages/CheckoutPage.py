from Resources.Locators.locators import CheckoutPageLocators as locator
import time
from Pages.BasePage.Page import Page
from selenium.webdriver.support.ui import Select
from selenium.webdriver.chrome.webdriver import WebDriver


class CheckoutPage(Page):

    # def __init__(self, driver: WebDriver):
    #     self.driver = driver

    def enter_user_details(self, data):
        cc_number = self.driver.find_element_by_xpath(locator.CC_NUMBER)
        cc_number_val = data['cc_no']
        cc_number.send_keys(cc_number_val)

        mm_yy = self.driver.find_element_by_xpath(locator.MMYY)
        mm_yy_val = data['mm_yy']
        mm_yy.send_keys(mm_yy_val)

        cvv = self.driver.find_element_by_xpath(locator.CVV)
        cvv_val = data['cvv']
        cvv.send_keys(cvv_val)

        time.sleep(5)

        donor_name = self.driver.find_element_by_xpath(locator.DONOR_NAME)
        donor_name_val = data['donor_name']
        donor_name.send_keys(donor_name_val)

        donor_email = self.driver.find_element_by_xpath(locator.DONOR_EMAIL)
        donor_email_val = data['donor_email']
        donor_email.send_keys(donor_email_val)

        donor_email_confirm = self.driver.find_element_by_xpath(locator.DONOR_CONFIRM_EMAIL)
        donor_email_confirm_val = data['donor_email']
        donor_email_confirm.send_keys(donor_email_confirm_val)

        state_code = data['state_code']

        select_state = self.driver.find_element_by_xpath(locator.STATE_DD)
        state_options = Select(select_state)
        state_options.select_by_value(state_code)

        CheckoutPage.click_pay_now(self)
        time.sleep(20)

    def click_pay_now(self):
        pay_now = self.driver.find_element_by_xpath(locator.PAY_NOW)
        pay_now.click()

    def select_payment_option(self, option):
        print(option, type(option), " TTTTTT")
        if option is "Stripe":
            credit_card = self.driver.find_element_by_xpath(locator.STRIPE_CC_RADIO)
            print("into Stripe select function")
            if credit_card.is_selected():
                print("credit_card option is already selected...")

            else:
                credit_card.click()
                print("credit_card option is selected...")

        if option is "Paypal":
            print("into paypal select function")
            paypal = self.driver.find_element_by_xpath(locator.PAYPAL_RADIO)
            time.sleep(15)

            if paypal.is_selected():
                print("Paypal option is already selected...")

            else:
                paypal.click()
                print("Paypal option is selected...")

        else :
            print("Payment option invalid")

    def donate_with_paypal(self):
        pay_paypal = self.driver.find_element_by_xpath(locator.PAY_WITH_PAYPAL)
        pay_paypal.click()

    def select_tip(self, tip_in_percent):
        print("into select_tip select function")
        select_dd = self.driver.find_element_by_xpath(locator.TIP_SELECT)
        select_tip = Select(select_dd)
        select_tip.select_by_value(tip_in_percent)
        time.sleep(5)




