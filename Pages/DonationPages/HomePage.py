from Resources.Locators.locators import HomePageLocators as locator
from Pages.BasePage.Page import Page

from Utility.WebElement import WebElement

from selenium.common.exceptions import NoSuchElementException


class HomePage(Page):

    #def __init__(self, driver):
    #    self.driver = driver

    def click_donate_now(self):
        try:
            donate_now = self.driver.find_element_by_xpath(locator.DONATE_NOW)
            WebElement.click_element(donate_now, self.driver)

        except NoSuchElementException:
            HomePage.close_newsletter_signup_popup(self)
            HomePage.click_donate_now(self)

    def login_to_user_account(self, data):
        HomePage.click_user_icon(self)

        active_tab = HomePage.check_active_tab(self)

        if active_tab is "signin":
            HomePage.enter_creds(self, data)
            HomePage.click_submit()

        else:
            HomePage.click_login_tab()
            HomePage.enter_creds(self, data)
            HomePage.click_submit()

    def enter_creds(self, data):
        email = self.driver.find_element_by_xpath(locator.EMAIL)
        email.send_keys(data.login_email)

        passwd = self.driver.find_element_by_xpath(locator.PASSWORD)
        passwd.send_keys(data.login_pass)

    def click_submit(self):
        submit = self.driver.find_element_by_xpath(locator.SUBMIT)
        submit.submit()

    def click_user_icon(self):
        user_icon = self.driver.find_element_by_xpath(locator.USER_ICON)
        user_icon.click()

    def click_login_tab(self):
        login_tab = self.driver.find_element_by_xpath(locator.SIGN_IN_TAB)
        login_tab.click()

    def click_register_tab(self):
        register_tab = self.driver.find_element_by_xpath(locator.SIGN_UP_TAB)
        register_tab.click()

    def check_active_tab(self):
        active_tab = self.driver.find_element_by_xpath(locator.SIGNIN_SIGNUP_ACTIVE_TAB)
        return active_tab.get_attribute("data-name")

    def sign_up_for_newsletter(self):
        pass

    def close_newsletter_signup_popup(self):
        close_newsletter = self.driver.find_element_by_xpath(locator.NEWSLETTER_POPUP_CLOSE)
        print("On the newsletter_signup_popup")
        close_newsletter.click()

    def enter_details_for_newsletter_subscribe(self, data):
        pass






