from Resources.Locators.locators import PetDetailsPageLocators as locator
from selenium.webdriver import Chrome
import time
from selenium.webdriver.common.action_chains import ActionChains
from Pages.BasePage.Page import Page
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

from selenium.common.exceptions import ElementNotInteractableException

from Utility.WebElement import WebElement


class PetDetailsPage(Page):

    # def __init__(self, driver: Chrome):
    #     self.driver = driver

    def donate_amount(self, amount):

        PetDetailsPage.click_donate_now(self)
        time.sleep(10)

        PetDetailsPage.get_rescue_info(self)

        actions_chains = ActionChains(self.driver)

        if amount == 15:
            print("Donating $15")
            amt = self.driver.find_element_by_xpath(locator.PAY_15)
            actions_chains.move_to_element(amt)
            time.sleep(5)
            self.driver.execute_script("window.scrollTo(0, 1400)") 
            time.sleep(3)
            actions_chains.click(amt)
            actions_chains.perform()

        elif amount == 35:
            print("Donating $35")
            amt = self.driver.find_element_by_xpath(locator.PAY_35)
            actions_chains.move_to_element(amt)
            time.sleep(5)
            self.driver.execute_script("window.scrollTo(0, 1400)") 
            time.sleep(3)
            actions_chains.click(amt)
            actions_chains.perform()

        elif amount == 55:
            print("Donating $55")
            amt = self.driver.find_element_by_xpath(locator.PAY_55)
            actions_chains.move_to_element(amt)
            time.sleep(5)
            self.driver.execute_script("window.scrollTo(0, 1400)") 
            time.sleep(3)
            actions_chains.click(amt)
            actions_chains.perform()

        elif amount == 105:
            print("Donating $105")
            amt = self.driver.find_element_by_xpath(locator.PAY_105)
            actions_chains.move_to_element(amt)
            time.sleep(5)
            self.driver.execute_script("window.scrollTo(0, 1400)") 
            time.sleep(3)
            actions_chains.click(amt)
            actions_chains.perform()

        else:
            if type(amount) is str:
                print("Donating $"+amount)

            if type(amount) is int:
                print("Donating $"+str(amount))
            other_button = self.driver.find_element_by_xpath(locator.PAY_OTHER_BUTTON)
            actions_chains.move_to_element(other_button)
            time.sleep(5)
            self.driver.execute_script("window.scrollTo(0, 1400)") 
            time.sleep(3)
            other_button.click()

            amt = self.driver.find_element_by_xpath(locator.PAY_OTHER_INPUT)
            amt.send_keys(amount)

        PetDetailsPage.click_donate(self)

        ''' action_chains = ActionChains(self.driver)
        action_chains.move_to_element(donate).perform()
        action_chains.double_click(donate).perform() '''

    def donate_product(self, donate_product_name):
        time.sleep(10)
        print("Searching product :"+donate_product_name+" in the Wishlist")

        product_list = self.driver.find_elements_by_xpath(locator.PRODUCT_NAME_LIST)

        flag = 0

        for product_name in product_list:
            if donate_product_name == product_name.text and product_name.text is not None:
                print(donate_product_name+" is donated...")
                product = self.driver.find_element_by_xpath("//div[contains(text(),'"+donate_product_name+"')]/parent::div/parent::div/following-sibling::div[@class='shopify-product-card__qty-update'])")
                product.click()
                flag = 1
                break

        if flag == 1:
            print("Product is added to Cart!")

        else:
            print("Product is not there in Wishlist ! So, can't add")

    def click_donate_now(self):
        time.sleep(3)
        if self.driver.find_element_by_xpath(locator.DONATE_NOW).is_displayed():
            donate_now = self.driver.find_element_by_xpath(locator.DONATE_NOW)
            WebElement.click_element(donate_now, self.driver)
            time.sleep(3)

    def click_donate(self):
        """
        time.sleep(2)
        donate = self.driver.find_element_by_xpath(locator.DONATE)
        actions = Actions(self.driver)
        actions.move_to_element(donate)
        WebDriverWait(self.driver, 15).until(
            EC.element_to_be_clickable((By.XPATH, "//button[contains(text(),'DONATE')]/parent::a")))
        donate.click()
        """

        donate = self.driver.find_element_by_xpath(locator.DONATE)

        actions = ActionChains(self.driver)
        actions.move_to_element(donate)
        WebDriverWait(self.driver, 15).until(
            EC.element_to_be_clickable((By.XPATH, locator.DONATE_BUTTON_LINK)))
        actions.move_to_element(donate)

        self.driver.execute_script("arguments[0].click();", donate)

    def get_rescue_info(self):
        rescue_name = self.driver.find_elements_by_xpath(locator.RESCUE_NAME)

        rescue_address = self.driver.find_elements_by_xpath(locator.RESCUE_ADD)

        return rescue_name, rescue_address

    def click_checkout(self):
        try:
            checkout_button = self.driver.find_element_by_xpath(locator.CHECKOUT)
            print("clcking checkout")
            #checkout_button.click()
            WebElement.click_element(checkout_button, self.driver)

        except ElementNotInteractableException:
            # print("ElementNotInteractableException occurred")
            PetDetailsPage.open_cart(self)
            time.sleep(5)
            checkout_button = self.driver.find_element_by_xpath(locator.CHECKOUT)
            print("clicking checkout_button")
            checkout_button.click()

    def get_pet_name(self):
        pet_name = self.driver.find_element_by_xpath(locator.PET_NAME)
        return pet_name.text

    def get_campaign_name(self):
        campaign_name = self.driver.find_element_by_xpath(locator.CAMPAIGN_TITLE)
        return campaign_name.text

    def update_cash_donation(self, val):
        pass                       # in cart

    def update_product_quantity(self, val):
        pass

    def open_cart(self):
        cart_icon = self.driver.find_element_by_xpath(locator.CART_ICON)
        # cart_icon.click()
        WebElement.click_element(cart_icon, self.driver)

    def close_cart(self):
        close_cart = self.driver.find_element_by_xpath(locator.CART_CLOSE)
        close_cart.click()

    def remove_donation_from_cart(self):
        pass

    def remove_product_from_cart(self):
        pass

    def verify_cart_items(self):
        pass

    def get_cash_donation_in_cart(self):  # Need to update , not working as expected
        cash_donation = self.driver.find_elements_by_xpath(locator.CART_ITEMS_PRICE_LIST)

        for i in cash_donation:
            print(i.get_attribute("text()"))

        print("........"+cash_donation[0].text.strip())
        return cash_donation[0].text.strip()

    def get_no_of_cart_items(self):
        cart_items_list = self.driver.find_elements_by_xpath(locator.ALL_CART_ITEMS)
        return cart_items_list.count()

    def get_all_cart_items(self):
        cart_items = self.driver.find_elements_by_xpath(locator.ALL_CART_ITEMS)

        for i in cart_items:
            cart_items_list = cart_items.__getitem__(i).__getattribute__("text")

        return cart_items_list

    def click_adopt_now(self):
        pass










