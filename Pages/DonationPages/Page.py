from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.webdriver import WebDriver


class Page:

    def __init__(self, driver=None):
        self.set_driver(driver)

    def set_driver(self, driver):
        self.driver = driver

    def scroll_down(self):
        print("into scroll down")
        donate = self.driver.scroll_to_offset
        donate.click()

    def wait(self, time_seconds, element):
        wait = WebDriverWait(self.driver, time_seconds)
        wait.until(EC.element_to_be_clickable((By.XPATH, element)))

    def get_title(self):
        title = self.driver.title
        return title












