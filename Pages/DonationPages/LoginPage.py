from Pages.BasePage.Page import Page
from Resources.Locators.locators import LoginPage as locator
from selenium.webdriver.chrome.webdriver import WebDriver


class LoginPage(Page):

    def __init__(self, driver: WebDriver):
        self.driver = driver

    def click_login_button(self):
        login = self.driver.find_element_by_xpath(locator.LOG_IN)
        login.click()

    def enter_creds(self, email_id, passwd):
        email = self.driver.find_element_by_xpath(locator.EMAIL_ID)
        email.send_keys(email_id)

        password = self.driver.find_element_by_xpath(locator.PASSWORD)
        password.send_keys(passwd)

    def login_to_user_acc(self, data):
        LoginPage.enter_creds(self, data.email, data.passwd)
        LoginPage.click_login_button(self)
