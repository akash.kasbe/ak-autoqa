from Resources.Locators.locators import PaypalLoginPageLocators as locator
from Pages.BasePage.Page import Page


class PaypalLoginPage(Page):

    # def __init__(self, driver):
    #     self.driver = driver

    def enter_email(self, email_id):
        email = self.driver.find_element_by_xpath(locator.PAYPAL_EMAIL)
        email.send_keys(email_id)

    def enter_password(self, password):
        passwd = self.driver.find_element_by_xpath(locator.PAYPAL_PASS)
        passwd.send_keys(password)

    def click_login(self):
        login = self.driver.find_element_by_xpath(locator.PAYPAL_LOGIN)
        login.click()

    def click_next(self):
        next_button = self.driver.find_element_by_xpath(locator.PAYPAL_NEXT)
        next_button.submit()

    def click_continue(self):
        continue_button = self.driver.find_element_by_xpath(locator.PAYPAL_CONTINUE)
        continue_button.click()

    def login_to_paypal(self, data):
        PaypalLoginPage.enter_email(self, data.email)
        PaypalLoginPage.click_next(self)
        PaypalLoginPage.enter_password(self, data.passwd)
        PaypalLoginPage.click_login(self)
        PaypalLoginPage.click_continue(self)




